import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

//IMPLEMENTAR FONTAWESOME MEJOR :D
const Tabla = (props) => {
   const ponerFilas = () => props.usuarios.map((usuario, key) => (
        <tr key={usuario.id}>
          <td>{usuario.name}</td>
          <td>{usuario.email}</td>
          <td>{usuario.website}</td>
          <td>
            <Link to={`/publicaciones/${key}`}>
              <i className="far fa-eye"></i>
            </Link>
          </td>
        </tr>
      ));

   return (
      <div>
         <table className="table">
          <thead>
            <tr>
              <th>Nombre</th>
              <th>Correo</th>
              <th>Enlace</th>
            </tr>
          </thead>
          <tbody>
            { ponerFilas() }
          </tbody>
        </table>
      </div>
   )
}

const mapStateToProps = (state) => {
   return state.usuariosReducer;
};

export default connect(mapStateToProps)(Tabla);
