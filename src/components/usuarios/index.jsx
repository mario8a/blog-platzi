import React, { Component } from 'react';
import { connect } from 'react-redux';
//Actions
// import { traerTodos } from './../../actions/usuariosActions';
import * as usuariosActions from './../../actions/usuariosActions';
import { Spinner } from '../general/Spinner';
import { ErrorFatal } from './../general/ErrorFatal';
import Tabla  from './Tabla';

class Usuarios extends Component {


  componentDidMount() {
    //Si hay usuarios no hagas nada
    if (!this.props.usuarios.length) {
      this.props.traerTodos();
    }
  }

  ponerContenido = () => {

    if (this.props.cargando) {
      return (
        <Spinner />
      );
    }

    if (this.props.error) {
      return <ErrorFatal mensaje={this.props.error}/>
    }

    return (
      <div className="table_container">
        <Tabla />
      </div>
    )
  }

  

  render() {
    // console.log(this.props); //Aqui ya se puede ver el estado global de la app, entre ellos los actions
    // console.log(this.props.cargando);
    // console.log(this.props.error);
    return (
      <div>
        <h1 className="text-center m-4"> Usuarios </h1>
        { this.ponerContenido() }
      </div>
    )
  }
}
//Deberia ser el store
const mapStateToProps = (state) => {
  return state.usuariosReducer;
}
//usuariosActions - TraerTodos
// export default connect(mapStateToProps, { traerTodos } )(Usuarios);
export default connect(mapStateToProps, usuariosActions  )(Usuarios);