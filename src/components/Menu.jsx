import React from "react";
import { Link } from "react-router-dom";

export const Menu = () => (
   <nav id="menu" className="navbar navbar-expand-md navbar-dark bg-dark">
      <div className="container-fluid">
         <h3 className="navbar-brand">
            Platzi Blog
         </h3>
         <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
         >
            <span className="navbar-toggler-icon"></span>
         </button>
         <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav">
               <li className="nav-item">
                  <Link className="nav-link active" aria-current="page" to="/">
                     Usuarios
                  </Link>
               </li>
               <li className="nav-item">
                  <Link className="nav-link" to="/tareas">
                     Tareas
                  </Link>
               </li>
            </ul>
         </div>
      </div>
   </nav>
);
