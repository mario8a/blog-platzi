import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as tareasActions from '../../actions/tareasActions';
import { Spinner } from './../general/Spinner';
import { ErrorFatal } from './../general/ErrorFatal';
import { Link } from 'react-router-dom';

class Tareas extends Component {

   componentDidMount() {
      //Si no hay tareas,
      if (!Object.keys(this.props.tareas).length) {
         this.props.traerTodas();
      }
   }

   componentDidUpdate() {
      const { tareas, cargando, traerTodas } = this.props;
      if (!Object.keys(tareas).length && !cargando) {
         traerTodas();
      }
   }

   mostrarContenido = () => {
      const { tareas, cargando, error } = this.props;

      if (cargando) {
        return <Spinner />
      };

      if (error) {
         return <ErrorFatal mensaje={error}/>
      }

      return Object.keys(tareas).map((usu_id) => (
         <div key={usu_id}>
            <h2> Usuario {usu_id} </h2>
            <div className="">
               { this.ponerTareas(usu_id) }
            </div>
         </div>
      ))
   };

   ponerTareas = (usu_id) => {
      const { tareas, cambioCheck, eliminar } = this.props;
      const por_usuario = {
         ...tareas[usu_id]
      };

      return Object.keys(por_usuario).map((tar_id) => (
         <div key={tar_id} className="d-flex bd-highlight align-items-center">
            <div className="p-2 bd-highlight">
               <input
                  className="form-check-input mx-2"
                  type="checkbox" 
                  defaultChecked={por_usuario[tar_id].completed}
                  onChange={() => cambioCheck(usu_id, tar_id)}
               />
            </div>
            <div className="p-2 bd-highlight">
               {
                  por_usuario[tar_id].title
               }
            </div>
            <div className="ms-auto p-2 bd-highlight">
               <button className="btn btn-outline-warning mx-2">
                  <Link to={`/tareas/guardar/${usu_id}/${tar_id}`}>
                     <i className="far fa-edit"></i>
                  </Link>
               </button>
               <button className="btn btn-outline-danger" onClick={() => eliminar(tar_id)} >
                  <i className="far fa-trash-alt"></i>
               </button>
            </div>
         </div>
      ))
   }

   render() {
      return (
         <div className="m-3">
            <Link className="agregar-btn" to="/tareas/guardar">
               <button type="button" className="btn btn-primary">
                     Agregar
               </button>
            </Link>
            { this.mostrarContenido() }
         </div>
      );
   }
}

const mapStateToProps = ({tareasReducer}) => tareasReducer;

export default connect(mapStateToProps, tareasActions)(Tareas);
