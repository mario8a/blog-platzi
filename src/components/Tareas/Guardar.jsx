import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

import * as tareasActions from "../../actions/tareasActions";
import { ErrorFatal } from "../general/ErrorFatal";
import { Spinner } from "../general/Spinner";

class Guardar extends Component {
   componentDidMount() {
      //verificar si tiene o no los parametros de la url
      const {
         match: {
            params: { usu_id, tar_id },
         },
         tareas,
         cambioUsuarioId,
         cambioTitulo,
         limpiarForma,
      } = this.props;

      if (usu_id && tar_id) {
         const tarea = tareas[usu_id][tar_id];
         cambioUsuarioId(tarea.userId);
         cambioTitulo(tarea.title);
      } else {
         limpiarForma();
      }
   }

   cambioUsuarioId = (e) => {
      this.props.cambioUsuarioId(e.target.value);
   };

   cambioTitulo = (e) => {
      this.props.cambioTitulo(e.target.value);
   };

   guardar = () => {
      const {
         usuario_id,
         titulo,
         agregar,
         match: {
            params: { usu_id, tar_id },
         },
         tareas,
         editar,
      } = this.props;

      const nueva_tarea = {
         userId: usuario_id,
         title: titulo,
         completed: false,
      };
      if (usu_id && tar_id) {
         const tarea = tareas[usu_id][tar_id];
         const tarea_editada = {
            ...nueva_tarea,
            completed: tarea.completed,
            id: tarea.id,
         };
         editar(tarea_editada);
      } else {
         agregar(nueva_tarea);
      }
   };

   deshabilitar = () => {
      const { usuario_id, titulo, cargando } = this.props;

      if (cargando) {
         return true;
      }
      if (!usuario_id || !titulo) {
         return true;
      }
      return false;
   };

   mostrarAction = () => {
      const { error, cargando } = this.props;
      if (cargando) {
         return <Spinner />;
      }

      if (error) {
         return <ErrorFatal mensaje={error} />;
      }
   };

   render() {
      return (
         <div className="container p-5">
            {this.props.regresar ? <Redirect to="/tareas" /> : ""}
            <h1>Guardar Tarea</h1>
            <div>
               <form>
                  <div className="mb-3">
                     <label className="form-label">Usuario id</label>
                     <input
                        type="number"
                        className="form-control"
                        value={this.props.usuario_id}
                        onChange={this.cambioUsuarioId}
                     />
                  </div>
                  <div className="mb-3">
                     <label for="exampleInputPassword1" className="form-label">
                        Titulo
                     </label>
                     <input
                        className="form-control"
                        id="exampleInputPassword1"
                        type="text"
                        value={this.props.titulo}
                        onChange={this.cambioTitulo}
                     />
                  </div>
                  <button 
                     type="submit" 
                     className="btn btn-primary" 
                     onClick={this.guardar} 
                     disabled={this.deshabilitar()}>
                     Guardar tarea
                  </button>
               </form>
            </div>
         </div>
      );
   }
}

const mapStateToProps = ({ tareasReducer }) => tareasReducer;

export default connect(mapStateToProps, tareasActions)(Guardar);
