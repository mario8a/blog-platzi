import React, { Component } from 'react';
import { Menu } from './Menu';

class Layout extends Component {
   render() {
      const { children } = this.props
      return (
         <>
            <Menu />
            <div className="container">
               {children}
            </div>
         </>
      );
   }
}

export default Layout;
