import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Spinner } from './../general/Spinner';
import { ErrorFatal } from './../general/ErrorFatal';

import * as usuariosActions from '../../actions/usuariosActions';
import * as publicacionesActions from '../../actions/publicacionesActions';
import Comentarios from './Comentarios';

const { traerTodos: usuariosTraerTodos } = usuariosActions;
const { 
   traerPorUsuario: publicacionestraerPorUsuario, 
   abrirCerrar,
   traerComentarios 
} = publicacionesActions;

class Publicaciones extends Component {

   async componentDidMount() {
      //Destructurando todo lo que no tiene que ver con el estado
      const { 
         usuariosTraerTodos,
         publicacionestraerPorUsuario,
         match: { params: {key} }
       } = this.props;
      //Primero trae los usuarios y despues las publicaciones
      if (!this.props.usuariosReducer.usuarios.length) {
         //Si no tiene usuarios el arreglo, traerlos
         await usuariosTraerTodos();
      }
      //Manejando el error si no exiten usuarios
      if (this.props.usuariosReducer.error) {
         return;
      }
      //publicaciones_key -> publicacionesIndex
      if (!('publicaciones_key' in this.props.usuariosReducer.usuarios[key])) {
         publicacionestraerPorUsuario(key);
      }
   }

   ponerUsuario = () => {
      const { 
         usuariosReducer, 
         match: { params: {key} } 
      } = this.props;

      if (usuariosReducer.error) {
         return <ErrorFatal mensaje={usuariosReducer.error} />
      }

      if (!usuariosReducer.usuarios.length ||  usuariosReducer.cargando) {
         return <Spinner />
      }
      //Si todo sale exitoso
      const nombre = usuariosReducer.usuarios[key].name;
      return (
         <h1 className="mt-3 mb-5">
            Publicaciones de {nombre}
         </h1>
      )
   };

   ponerPublicaciones = () => {
      const {
         usuariosReducer,
         usuariosReducer: {usuarios},
         publicacionesReducer,
         publicacionesReducer: { publicaciones },
         match: { params: {key} }
      } = this.props;

      if (!usuarios.length) return;
      if (usuariosReducer.error) return;
      //Si la info si paso
      if (publicacionesReducer.cargando) {
         return <Spinner />
      };
      if (publicacionesReducer.error) {
         return <ErrorFatal mensaje={publicacionesReducer.error}/>
      };

      if (!publicaciones.length) return;
      if (!('publicaciones_key' in usuarios[key])) return;
      
      const { publicaciones_key } = usuarios[key];
      if (!publicaciones[publicaciones_key]) return;
      
      return this.mostrarInfo(
         publicaciones[publicaciones_key],
         publicaciones_key
      )
   };

   mostrarInfo = (publicaciones, pub_key) => (
      publicaciones.map((publicacion, com_key) => (
         <div
            key={publicacion.id} 
            className='card publicaciones-card' 
            onClick={() => this.mostrarComentarios(pub_key, com_key, publicacion.comentarios)}
         >
            <div className="card-body">
               <h2 className="card-title">
                  { publicacion.title }
               </h2>
               <p className="card-text">
                  {publicacion.body}
               </p>
               {
                  (publicacion.abierto) ? <Comentarios comentarios={publicacion.comentarios}/> : ''
               }
            </div>
         </div>
      ))
   );

   mostrarComentarios = (pub_key, com_key, comentarios) => {
      this.props.abrirCerrar(pub_key, com_key);
      if (!comentarios.length) {
         this.props.traerComentarios(pub_key, com_key);
      }
   };

   render() {
      // console.log('Aqui debe',this.props);
      return (
         <div>
            { this.ponerUsuario() }
            { this.ponerPublicaciones() }
         </div>
      );
   }
}

const mapStateToProps = ({usuariosReducer, publicacionesReducer}) => {
   return {
      usuariosReducer,
      publicacionesReducer
   };
};

const mapDispatchToProps = {
	usuariosTraerTodos,
	publicacionestraerPorUsuario,
   abrirCerrar,
   traerComentarios
};

export default connect(mapStateToProps, mapDispatchToProps)(Publicaciones);
