import React from 'react';
import { connect } from 'react-redux';
import { ErrorFatal } from '../general/ErrorFatal';
import { Spinner } from '../general/Spinner';


const Comentarios = (props) => {
   const { comentarios } = props;
   console.log(props);
   //Si no existen comentarios y cargando esta en true

   if (props.com_error) {
      return <ErrorFatal mensaje={props.com_error} />
   }

   if (props.com_cargando && !props.comentarios.length) {
      return <Spinner />
   };

   const ponerComentarios = () => (
      comentarios.map((comentario) => (
         <li key={comentario.id}>
            <b><u> {comentario.email} </u></b>
            <br />
            { comentario.body }
         </li>
      ))
   )

   return (
      <ul className="comments-list">
         { ponerComentarios() }
      </ul>
   )
};

const mapStateToProps = ({publicacionesReducer}) => publicacionesReducer;

export default connect(mapStateToProps)(Comentarios);