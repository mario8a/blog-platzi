import React from 'react';
import errorImg from '../../assets/error500.png';

export const ErrorFatal = ({mensaje}) => {
   return (
      <div className="img-err">
        <img src={errorImg} alt="" width="100%"/>
        <p> {mensaje} </p>
      </div>
   )
}
