import React from 'react'
import { BrowserRouter, Route } from 'react-router-dom'
import Layout from './Layout';
import Usuarios from './usuarios';
import Publicaciones from './publicaciones/index';
import Tareas from './Tareas/index';
import TareasGuardar from './Tareas/Guardar';

export const App = () => (
    <BrowserRouter>
      <Layout>
        <Route exact path="/" component={Usuarios}/>
        <Route exact path="/tareas" component={Tareas}/>
        <Route exact path="/publicaciones/:key" component={Publicaciones}/>
        <Route exact path="/tareas/guardar" component={TareasGuardar}/>
        <Route exact path="/tareas/guardar/:usu_id/:tar_id" component={TareasGuardar}/>
      </Layout>
    </BrowserRouter>
);