import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reduxThunk from 'redux-thunk';
//Reducer
import reducers from './reducers';
//Components
import { App } from './components/App';
//Styles
import './css/index.css';
import './css/iconos.css';


const store = createStore(
  reducers,  //Todos los reducers
  {}, //*Estado inicial
  applyMiddleware(reduxThunk)
);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
